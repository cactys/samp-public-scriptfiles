# [![SA:MP](doc/res/sa-mp.png)](http://sa-mp.com) server scripting files

This is a collection of all script files that were published with
[SA:MP](http://sa-mp.com) server files in the past - compiled into a git
repository to observe changes throughout the years.

However, this repository does not contain any kind of binary file for
various obvious reasons. It also does not include files from
<abbr title="Release Candidate">RC</abbr> versions. The timestamps for
the commits were taken from the upload timestamps of the published
archives.

> I don't claim copyright for any of the files inside [`files/`](files).
